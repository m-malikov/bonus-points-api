### Bonus points API

#### Running app
To start web app and database with docker-compose run:
`docker-compose up --build`.

Web app is exposed to host network on `http://localhost:8000` and Postgres database is exposed to host network on host 5432.

To remove docker containers run `docker-compose stop`.

It is possible to run some simple requests by running file `run_test_requests.py` on host machine. It requires no extra dependencies.

Some example requests using curl:

`curl -X 'POST' 'http://localhost:8000/api/v1/create_user'`

`curl -X 'PATCH' 'http://localhost:8000/api/v1/topup?user_id=d7c8407f-058f-47d5-ba14-c14f08e5dfd3&amount=100'`.

More requests could be generated via openapi from definition file `opeapi.yaml`

#### Thoughts behind technical decisions
**Why flask and not something async?**

There isn't a lot of IO happening in the API calls. Single HTTP request requires at most two requests to the database which isn't much. Using several workers under gunicorn allows API to process multiple concurrent requests.

**Why postgres and not a key-value storage or some other NoSQL?**

Postgres looks like a safe bet since it can process such queries reasonably fast while still providing good consistency. We could totaly use some other type of persistent storage though.

**Why psycopg2 without ORM?**

We only have single table so it's hard to say that this data is relational. Also, there isn't any http methods returning "objects", only current balance as number or new user id. Thus object-relation mapping looks really out of place here. Another reason is that it's highly likely that we will drill down to the details of database and backend interaction during discussion. It would be much easier to talk if those details are easy to see directly.
