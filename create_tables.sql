-- More complex project would probably require some 
-- migration management but I've decided to stop at a
-- simple init script.
CREATE TABLE users(user_id uuid PRIMARY KEY, balance bigint);