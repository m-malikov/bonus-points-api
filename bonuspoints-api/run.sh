#!/bin/bash

source venv/bin/activate
sleep 2
cd app
gunicorn -w 4 --access-logfile='-' "app:create_app('prod')"