import os


class Config:
    DATABASE_CONNECTION_STRING = os.environ.get("DATABASE_CONNECTION_STRING")


class DevConfig(Config):
    DEBUG = True


class ProdConfig(Config):
    pass


config = {"dev": DevConfig, "prod": ProdConfig}
