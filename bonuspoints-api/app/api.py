from uuid import UUID
from flask import Blueprint, Response, request

import db

api_blueprint = Blueprint("api", __name__)

USER_NOT_FOUND_RESPONSE = Response("User not found", status=404)
NOT_ENOUGH_POINTS_RESPONSE = Response("Not enough points", status=400)
INVALID_USER_ID_RESPONSE = Response("user_id is not a valid uuid4", status=400)
INVALID_AMOUNT_RESPONSE = Response("Invalid amount", status=400)


def is_valid_uuid4(s):
    try:
        UUID(s, version=4)
        return True
    except (ValueError, TypeError):
        return False


def is_valid_amount(amount):
    try:
        amount = int(amount)
        return amount > 0
    except (ValueError, TypeError):
        return False


@api_blueprint.route("/balance", methods=["GET"])
def get_balance():
    user_id = request.args.get("user_id")

    if not is_valid_uuid4(user_id):
        return INVALID_USER_ID_RESPONSE

    try:
        balance = db.get_balance(str(user_id))
        return Response(str(balance), status=200)
    except db.UserNotFoundError:
        return USER_NOT_FOUND_RESPONSE


@api_blueprint.route("/topup", methods=["PATCH"])
def topup():
    user_id = request.args.get("user_id")
    amount = request.args.get("amount")

    if not is_valid_uuid4(user_id):
        return INVALID_USER_ID_RESPONSE
    if not is_valid_amount(amount):
        return INVALID_AMOUNT_RESPONSE

    try:
        db.topup(str(user_id), amount)
        return Response("ok", 200)
    except db.UserNotFoundError:
        return USER_NOT_FOUND_RESPONSE


@api_blueprint.route("/withdraw", methods=["PATCH"])
def withdraw():
    user_id = request.args.get("user_id")
    amount = request.args.get("amount")

    if not is_valid_uuid4(user_id):
        return INVALID_USER_ID_RESPONSE
    if not is_valid_amount(amount):
        return INVALID_AMOUNT_RESPONSE

    try:
        db.withdraw(str(user_id), amount)
        return Response("ok", 200)
    except db.UserNotFoundError:
        return USER_NOT_FOUND_RESPONSE
    except db.NotEnoughPointsError:
        return NOT_ENOUGH_POINTS_RESPONSE


@api_blueprint.route("/transfer", methods=["PATCH"])
def transfer():
    user_from = request.args.get("user_from")
    user_to = request.args.get("user_to")
    amount = request.args.get("amount")

    if not is_valid_uuid4(user_from):
        return INVALID_USER_ID_RESPONSE
    if not is_valid_uuid4(user_to):
        return INVALID_USER_ID_RESPONSE
    if not is_valid_amount(amount):
        return INVALID_AMOUNT_RESPONSE
    if user_from == user_to:
        return Response("Can't transfer points to yourself", 400)

    try:
        db.transfer(str(user_from), str(user_to), amount)
        return Response("ok", 200)
    except db.UserNotFoundError:
        return USER_NOT_FOUND_RESPONSE
    except db.NotEnoughPointsError:
        return NOT_ENOUGH_POINTS_RESPONSE


@api_blueprint.route("/create_user", methods=["POST"])
def create_user():
    new_user_id = db.create_user()
    return Response(new_user_id, 201)
