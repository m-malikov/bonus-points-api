import uuid
from functools import wraps
from os import _exit
import logging

import psycopg2


# Instead of creating a global variable we could
# store it in flask app_context, or use a singleton object
# but just using a global variable looks simpler in this case

conn = None


def init_app(app):
    global conn
    conn = psycopg2.connect(app.config["DATABASE_CONNECTION_STRING"])


def handle_disconnect(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except psycopg2.InterfaceError:
            # If database is disconnected, shutdown the app
            # to restart it later. Real production app
            # would probably require more graceful approach
            _exit(1)

    return wrap


class UserNotFoundError(ValueError):
    pass


class NotEnoughPointsError(ValueError):
    pass


@handle_disconnect
def get_balance(user_id):
    # Returned value will be consistent as long as it is
    # not in the middle of another transaction
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_READ_COMMITTED)
    cursor = conn.cursor()
    try:
        cursor.execute(
            "SELECT balance FROM users WHERE user_id = %(user_id)s",
            {"user_id": user_id},
        )
        conn.commit()
    finally:
        conn.rollback()

    cnt = cursor.fetchone()
    if cnt is None:
        raise UserNotFoundError()
    return cnt[0]


# We expect the contention to be low
# (i.e. situations when multiple requests are trying
# to modify balance of one user to be rare).
#
# SERIALIZABLE transactions and retries on SerializationFailure
# will lock optimisticaly and provide both consistency
# and little overhead on most requests.

MAX_SERIALIZATION_RETRIES = 5


def retry_serialize(f):
    @wraps(f)
    def f_with_retry(*args, **kwargs):
        for i in range(MAX_SERIALIZATION_RETRIES):
            try:
                conn.set_isolation_level(
                    psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE
                )
                return f(*args, **kwargs)
            except psycopg2.errors.SerializationFailure:
                logging.warning(
                    "Retrying transaction (%s / %s)", i, MAX_SERIALIZATION_RETRIES
                )
            finally:
                conn.rollback()
        logging.error("Achieved max retries, aborting")
        raise psycopg2.errors.SerializationFailure()

    return f_with_retry


@retry_serialize
def topup(user_id, amount):
    cursor = conn.cursor()
    cursor.execute(
        "UPDATE users SET balance = balance + %(amount)s WHERE user_id = %(user_id)s",
        {"user_id": user_id, "amount": amount},
    )
    conn.commit()
    # using rowcount to avoid querying the database second time
    if cursor.rowcount == 0:
        raise UserNotFoundError()


@retry_serialize
def withdraw(user_id, amount):
    cursor = conn.cursor()
    cursor.execute(
        """
            UPDATE users SET balance = balance - %(amount)s
            WHERE user_id = %(user_id)s AND balance >= %(amount)s
        """,
        {"user_id": user_id, "amount": amount},
    )
    if cursor.rowcount == 1:
        conn.commit()
    else:
        cursor.execute(
            "SELECT COUNT(*) FROM users WHERE user_id = %(user_id)s",
            {"user_id": user_id, "amount": amount},
        )
        cnt_users = cursor.fetchone()[0]
        if cnt_users == 0:
            raise UserNotFoundError()
        else:
            raise NotEnoughPointsError()


@retry_serialize
def transfer(user_from, user_to, amount):
    cursor = conn.cursor()
    cursor.execute(
        """
            UPDATE users SET balance = balance - %(amount)s
            WHERE user_id = %(user_from)s AND balance >= %(amount)s
        """,
        {"user_from": user_from, "amount": amount},
    )
    if cursor.rowcount == 0:
        cursor.execute(
            "SELECT COUNT(*) FROM users WHERE user_id = %(user_from)s",
            {"user_from": user_from, "amount": amount},
        )
        conn.rollback()
        cnt_users = cursor.fetchone()[0]
        if cnt_users == 0:
            raise UserNotFoundError()
        else:
            raise NotEnoughPointsError()

    cursor.execute(
        "UPDATE users SET balance = balance + %(amount)s WHERE user_id = %(user_to)s",
        {"user_to": user_to, "amount": amount},
    )
    if cursor.rowcount == 0:
        conn.rollback()
        raise UserNotFoundError()
    conn.commit()


@retry_serialize
def create_user():
    try:
        new_id = str(uuid.uuid4())
        cursor = conn.cursor()
        cursor.execute(
            "INSERT INTO users(user_id, balance) VALUES (%(new_id)s, 0)",
            {"new_id": new_id},
        )
        conn.commit()
        return new_id
    finally:
        conn.rollback()
