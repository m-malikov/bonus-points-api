# There is no complex business logic in this application,
# so unit tests would probably give little value. Also we would 
# have to write mocks for database and web server.
# On the other hand, the whole application can be easily 
# run in docker-compose, making it real easy to write some
# simple "end-to-end" tests

import requests

hostname = "http://localhost:8000/api/v1"

def run_request(method, query, expected_code=None, expected_value=None):
    print(method, query)
    if method == "GET":
        request_function = requests.get
    elif method == "POST":
        request_function = requests.post
    elif method == "PATCH":
        request_function = requests.patch
    response = request_function(hostname + query)
    print(response.status_code, response.text)
    if expected_code is not None:
        assert response.status_code == expected_code
    if expected_value is not None:
        assert response.text == expected_value
    return response.status_code, response.text

if __name__ == "__main__":
    # simple requests for single user
    _, user1 = run_request("POST", "/create_user", 201)
    run_request("GET", "/balance?user_id=" + user1, 200, "0")
    run_request("PATCH", "/topup?user_id={}&amount=1001".format(user1), 200)
    run_request("GET", "/balance?user_id=" + user1, 200, "1001")
    run_request("PATCH", "/withdraw?user_id={}&amount=3000".format(user1), 400, "Not enough points")
    run_request("GET", "/balance?user_id=" + user1, 200, "1001")
    run_request("PATCH", "/withdraw?user_id={}&amount=1".format(user1), 200)
    run_request("GET", "/balance?user_id=" + user1, 200, "1000")

    run_request("PATCH", "/withdraw?user_id={}&amount=-1".format(user1), 400, "Invalid amount")
    run_request("PATCH", "/withdraw?user_id={}&amount=abd".format(user1), 400, "Invalid amount")
    run_request("PATCH", "/topup?user_id={}&amount=-100".format(user1), 400, "Invalid amount")
    run_request("PATCH", "/topup?user_id={}&amount=abd".format(user1), 400, "Invalid amount")

    # transfers
    _, user_a = run_request("POST", "/create_user", 201)
    _, user_b = run_request("POST", "/create_user", 201)
    run_request("PATCH", "/topup?user_id={}&amount=1000".format(user_a), 200)
    run_request("PATCH", "/topup?user_id={}&amount=100".format(user_b), 200)
    run_request("PATCH", "/transfer?user_from={}&user_to={}&amount=105".format(user_a, user_b), 200)
    run_request("GET", "/balance?user_id=" + user_a, 200, "895")
    run_request("GET", "/balance?user_id=" + user_b, 200, "205")
    run_request("PATCH", "/transfer?user_from={}&user_to={}&amount=3".format(user_b, user_a), 200)
    run_request("GET", "/balance?user_id=" + user_a, 200, "898")
    run_request("GET", "/balance?user_id=" + user_b, 200, "202")
    run_request("PATCH", "/transfer?user_from={}&user_to={}&amount=10000".format(user_b, user_a), 400, "Not enough points")
    run_request("PATCH", "/transfer?user_from={}&user_to={}&amount=-100".format(user_b, user_a), 400, "Invalid amount")
    run_request("PATCH", "/transfer?user_from={}&user_to={}&amount=10".format(user_a, user_a), 400, "Can't transfer points to yourself")

    # nonexisting users
    run_request("GET", "/balance?user_id=e257231b-3352-4664-b6eb-dd26e26e6bc5", 404, "User not found")
    run_request("PATCH", "/topup?user_id=f60ccaa8-41bc-4f88-bd8a-e0b6870369ee&amount=100", 404, "User not found")
    run_request("PATCH", "/withdraw?user_id=dd5fe28b-1fb8-48d4-bd6e-47f993c8440c&amount=100", 404, "User not found")
    _, user2 = run_request("POST", "/create_user", 201)
    run_request("PATCH", "/topup?user_id={}&amount=1000".format(user2), 200)
    run_request("PATCH", "/transfer?user_from={}&user_to=dd5fe28b-1fb8-48d4-bd6e-47f993c8440c&amount=1".format(user2), 404, "User not found")
    run_request("PATCH", "/transfer?user_to={}&user_from=dd5fe28b-1fb8-48d4-bd6e-47f993c8440c&amount=1".format(user2), 404, "User not found")
    
    print("Tests run successfully!")